<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Savvy\Cores\TaggableContract;

class PostsController extends Controller
{
    protected $taggable;

    public function __construct(TaggableContract $taggable)
    {
        $this->taggable = $taggable;
    }

    public function create()
    {
        return view('post.create');
    }
    public function store(Request $request)
    {
        $post  = \App\Post::find(1);
        $tag = $request->tag;
        $this->taggable->addTag($post,$tag);

    }
}

<?php

namespace Savvy\Cores;

interface TaggableContract {

    public function addTag($model,$tag);
}
<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/



$factory->define(Savvy\Branch\Branch::class, function (Faker\Generator $faker) {

    static $i = 1000;
    return [
        'id' => $i++,
        'name' => $faker->text(),
        'address' =>$faker->text(),
        'phone' =>$faker->text(),
        'created_by' => 1,


    ];
});




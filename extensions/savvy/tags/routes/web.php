<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Branch Management
Route::group([
		'namespace' => 'Savvy\Branch\Http\Controllers',
		'middleware' => ['web', 'auth', 'permissions']
	], 
	function($route) {

		$route->get('branch', [
			'as' => 'branch.list',
			'uses' => 'BranchController@index'
		]);

		$route->get('branch/create', [
			'as' => 'branch.create',
			'uses' => 'BranchController@create'
		]);

		$route->post('branch/store', [
			'as' => 'branch.store',
			'uses' => 'BranchController@store'
		]);

		$route->get('branch/{id}/edit', [
			'as' => 'branch.edit',
			'uses' => 'BranchController@edit'
		]);

		$route->post('branch/update/{id}', [
			'as' => 'branch.update',
			'uses' => 'BranchController@update'
		]);

		$route->get('branch/destroy/{id}', [
			'as'	=>	'branch.destroy',
			'uses'	=>	'BranchController@destroy',
		]);

		$route->get('branch/response', [
			'as' => 'branch.response',
			'uses' => 'BranchController@getBranchesResponse'
		]);
		
	}
);
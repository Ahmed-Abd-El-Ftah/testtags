<?php

namespace Savvy\Tags\Providers;

use Illuminate\Support\ServiceProvider;
use Savvy\Cores\TaggableContract;
use Savvy\Tags\TagsImpl;

class TagsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind(TaggableContract::class,function ($app){
            return new TagsImpl();
        });
    }
}
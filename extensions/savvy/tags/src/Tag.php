<?php

namespace Savvy\Tags;

use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{
    protected $fillable = ['taggable_type','taggable_id','tag'];
    protected $table = 'taggable';

}

<?php

namespace Savvy\Tags;

use Savvy\Cores\TaggableContract;


class TagsImpl implements TaggableContract
{
    public  function addTag($model,$tag)
    {
        return Tag::create([
            'taggable_type' => get_class($model),
            'taggable_id' => $model->id,
            'tag' => $tag

        ]);

    }

    public function getModelTags ($model)
    {
        Tag::where();
        return 'tags';
    }
}
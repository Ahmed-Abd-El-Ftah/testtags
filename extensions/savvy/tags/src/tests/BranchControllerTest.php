<?php


class BranchControllerTest extends BaseTest
{
    protected $user;

    public function setUpHock()
    {
        $this->user = \Savvy\User\User::find(1);
        \Cartalyst\Sentinel\Laravel\Facades\Sentinel::login($this->user);
    }

    public function test_index ()
    {

        factory(Savvy\Branch\Branch::class,1)->create();

        $branches = \Savvy\Branch\Branch::all();

         $this->call('GET','/branch');

         $this->assertViewHas('branches',$branches);

    }

    public function test_create ()
    {
       $this->visit(route('branch.create'))->see('add new branch');
    }

	 public function test_edit ()
    {
        $branch = factory(Savvy\Branch\Branch::class,1)->create(['created_by' => $this->user->id]);

        $this->call('GET','/branch/'.$branch->id.'/edit');

        $this->assertViewHas('branch');

    }

 
}

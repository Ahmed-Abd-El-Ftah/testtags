<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class BranchModelTest extends BaseTest
{

    public function test_creating_model_object()
    {
       $factory = factory(Savvy\Branch\Branch::class, 1)->make();

        $this->assertInstanceOf('Savvy\Branch\Branch',$factory);
    }

    public function test_scope_trimmed ()
    {

        $factory =factory(Savvy\Branch\Branch::class, 1)->make();

        $this->assertEquals(substr($factory->address,0,50).' ...', $factory->scopeTrimmedAddress());

        $factory->address = 'hello';

        $this->assertEquals('hello' , $factory->scopeTrimmedAddress());

    }

    public function test_user_have_many_branches ()
    {

     factory(Savvy\User\User::class, 1)->create()->each(function ($user)  {
                  $user->branches()->sync(factory(Savvy\Branch\Branch::class,10)->create()->pluck('id'));
        });

        $this->assertEquals(10,count(Savvy\User\User::find(1000)->branches));
    }

    public function test_events_morph_many ()
    {
        factory(Savvy\User\User::class, 1)->create()->each(function ($user)  {
            $user->events()->sync(factory(Savvy\Event\Event::class,5)->create()->pluck('id'));
        });

        $this->assertEquals(5,count(Savvy\User\User::find(1000)->events));

    }

}
